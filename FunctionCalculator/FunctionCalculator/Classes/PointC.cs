﻿namespace FunctionCalculator.Classes
{
    using System;
    using System.Drawing;

    public class PointC
    {
        private PointF point;
        private AverageEnum average;

        public PointC(float x, float y, AverageEnum average = AverageEnum.Equal)
        {
            this.point = new PointF(x, y);
            this.average = average;
        }

        public PointC(float x, float y, string average)
        {
            this.point = new PointF(x, y);
            this.average = (AverageEnum)Enum.Parse(typeof(AverageEnum), average, true);
        }

        public float X { get { return this.point.X; } }
        public float Y { get { return this.point.Y; } }
        public PointF Point { get { return this.point; } }
        public AverageEnum Average { get { return this.average; } }
    }

    public enum AverageEnum
    {
        Lesser = -1,
        Equal = 0,
        Greater = 1
    }
}
