﻿namespace FunctionCalculator.Classes
{
    using System.Collections.Generic;
    using System.Drawing;
    using Utils;

    public class FunctionCalculator
    {
        private Function function;
        private IEnumerable<PointC> hintPoints;

        public FunctionCalculator(IEnumerable<PointC> hintPoints)
        {
            this.function = new Function();
            this.hintPoints = hintPoints;
        }

        private Function Function { get { return this.function; } }

        public void Execute(int loopCount)
        {
            this.Function.Randomize();
            this.CalculateFunction(loopCount);
        }     

        private void CalculateFunction(int loopCount)
        {
            while (loopCount-- > 0)
            {
                foreach (PointC hintPoint in this.hintPoints)
                {
                    if (this.ClassifyPoint(hintPoint.Point) != hintPoint.Average)
                        this.AdjustFunction(hintPoint);
                }
            }
        }

        public AverageEnum ClassifyPoint(PointF point)
        {
            float fx = this.Function.Fx(point.X);
            return fx < point.Y ? AverageEnum.Greater : fx > point.Y ? AverageEnum.Lesser : AverageEnum.Equal;
        }

        private void AdjustFunction(PointC point)
        {
            this.function.M += point.Y / point.X > this.function.M ? 0.1F : -0.1F;
        }
    }
}
