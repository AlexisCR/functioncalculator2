﻿namespace FunctionCalculator.Classes
{
    using System;
    using Utils;

    public class Function
    {
        private float m;
        private float b;

        private Random random;

        public Function()
        {
            this.b = Constants.Values.B;
            this.random = new Random();
        }

        public float M
        {
            get { return this.m; }
            set { this.m = value; }
        }

        public float B
        {
            get { return this.b; }
            set { this.b = value; }
        }

        public void Randomize()
        {
            this.m = this.random.Next(0, Constants.Values.M_MAX + 1);
        }

        public float Fx(float x)
        {
            return this.m * x + this.b;
        }
    }
}
