﻿namespace FunctionCalculator.Utils
{
    using System.IO;

    public static class Constants
    {
        public static class Messages
        {
            public const string ENTER_VALUE = "Enter a value for {0} ({1}): ";
            public const string LOOP_COUNT = "Training loop count (min {0}): ";
            public const string VALUE_FORMAT = "    {0} ≤ {1} ≤ {2}: ";
            public const string WRONG_VALUE = "\nYou wrote an invalid value, please try again.\n";
            public const string POINT_STATUS = "\nThe " + X_NAME + "-" + Y_NAME + " combination is {0} than the average.\n";
            public const string X_NAME = "Age";
            public const string X_UNIT = "years";
            public const string Y_NAME = "Size";
            public const string Y_UNIT = "cm";
        }

        public static class Paths
        {
            private static readonly string DATA_FOLDER = Path.Combine("..", "..", "Data");
            public static readonly string POINTS_FILE = Path.Combine(DATA_FOLDER, "Points.xml");
        }

        public static class Values
        {
            public const int B = 0;
            public const int COORDINATE_MIN = 0;
            public const int LOOP_COUNT_MIN = 100;
            public const int M_MAX = 10;
            public const int X_MAX = 20;
            public const int Y_MAX = 200;
        }
    }
}
