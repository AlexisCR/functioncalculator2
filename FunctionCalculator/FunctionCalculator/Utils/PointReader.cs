﻿namespace FunctionCalculator.Utils
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using Classes;

    public static class PointReader
    {
        public static IEnumerable<PointC> ReadPoints()
        {
            return from pointElement in XDocument.Load(Constants.Paths.POINTS_FILE).Root.Elements()
                   select new PointC(
                       float.Parse(pointElement.Element("X").Value),
                       float.Parse(pointElement.Element("Y").Value),
                       pointElement.Element("Average").Value
                    );
        }
    }
}
