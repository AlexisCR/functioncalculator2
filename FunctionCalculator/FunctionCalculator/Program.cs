﻿namespace FunctionCalculator
{
    using System;
    using System.Drawing;
    using Classes;
    using Utils;

    public class Program
    {
        private static FunctionCalculator functionCalculator;

        public static void Main(string[] args)
        {
            int loopCount, x, y;
            functionCalculator = new FunctionCalculator(PointReader.ReadPoints());
        SetLoopCount:
            Console.Write(string.Format(Constants.Messages.LOOP_COUNT, Constants.Values.LOOP_COUNT_MIN));
            if (!int.TryParse(Console.ReadLine(), out loopCount) || loopCount < Constants.Values.LOOP_COUNT_MIN)
            {
                Console.WriteLine(Constants.Messages.WRONG_VALUE);
                goto SetLoopCount;
            }

            functionCalculator.Execute(loopCount);

            while (true)
            {
            SetX:
                if (!AskCoordinate(out x, Constants.Values.X_MAX, Constants.Messages.X_NAME, Constants.Messages.X_UNIT))
                {
                    Console.WriteLine(Constants.Messages.WRONG_VALUE);
                    goto SetX;
                }
            SetY:
                if (!AskCoordinate(out y, Constants.Values.Y_MAX, Constants.Messages.Y_NAME, Constants.Messages.Y_UNIT))
                {
                    Console.WriteLine(Constants.Messages.WRONG_VALUE);
                    goto SetY;
                }
                Console.WriteLine(string.Format(Constants.Messages.POINT_STATUS, functionCalculator.ClassifyPoint(new PointF(x, y)).ToString()));
            }            
        }

        private static bool AskCoordinate(out int coordinate, int max, string name, string unit)
        {
            Console.WriteLine(string.Format(Constants.Messages.ENTER_VALUE, name, unit));
            Console.Write(string.Format(
                Constants.Messages.VALUE_FORMAT, 
                Constants.Values.COORDINATE_MIN, 
                name,
                max
            ));
            return int.TryParse(Console.ReadLine(), out coordinate)
                && coordinate >= Constants.Values.COORDINATE_MIN
                && coordinate <= max;
        }
    }
}
